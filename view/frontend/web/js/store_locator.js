require([ 'jquery' ], function($){
	$(document).ready(function() {
		$(".store-links .item").hover(function(){		
			var itemId = $(this).attr('id');		
			$("#" + itemId + "-sd").css("display", "block");
			$(this).css("background-color", "black");
			$(this).css("color", "white");		
		}, function() {
			$(this).css("background-color", "white");
			$(this).css("color", "black");			
			$(".store-directions").css('display', 'none');
		});
	}); 
});